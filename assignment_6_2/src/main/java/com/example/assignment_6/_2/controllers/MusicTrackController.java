package com.example.assignment_6._2.controllers;

import com.example.assignment_6._2.data_access.MusicTrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MusicTrackController {

    private final MusicTrackRepository musicTrackRepository;
    public MusicTrackController(MusicTrackRepository musicTrackRepository) { this.musicTrackRepository = musicTrackRepository; }

    @GetMapping(value = "/")
    public String index(Model model) {
        model.addAttribute("tracks", musicTrackRepository.getRandomTracks(5));
        model.addAttribute("artists", musicTrackRepository.getRandomArtist(5));
        model.addAttribute("genres", musicTrackRepository.getRandomGenres(5));
        return "index";
    }

    @GetMapping(value = "/tracks", params = "trackName")
    public String getTrackByName(@RequestParam String trackName, Model model){
        model.addAttribute("tracks", musicTrackRepository.getTrackByName(trackName));
        return "view-tracks";
    }
}

