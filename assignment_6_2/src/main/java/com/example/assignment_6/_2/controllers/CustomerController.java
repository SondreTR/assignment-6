package com.example.assignment_6._2.controllers;

import com.example.assignment_6._2.data_access.CustomerRepository;
import com.example.assignment_6._2.models.Customer;
import com.example.assignment_6._2.models.CustomerCountry;
import com.example.assignment_6._2.models.CustomerSpender;
import com.example.assignment_6._2.models.Genre;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class CustomerController {

    // Configure some endpoints to manage crud
    private final CustomerRepository customerRepository;
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /*
     This first one just returns all the customers in the database
     it will return a CustomerShort object.
    */
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "api/customers", params = "name", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomerByName(@PathVariable String name){
        return customerRepository.getCustomersByName(name);
    }

    /*
     This returns a specific customer, based on a given Id.
     We use a path variable here to extract the Id.
    */
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerByPathId(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    /*
     This adds a new customer.
     It takes the new customer from the body of the request.
    */
    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    /*
     This updates an existing customer.
     It takes the new customer data from the body of the request.
     As well as taking the Id of the customer from the path variables, this is
     to do a check if the body matches the id. Just a layer of saftey.
    */
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    @GetMapping(value = "/api/customers/countries")
    public ArrayList<CustomerCountry> getCustomerCountByCountry() {
        return customerRepository.getCustomerCountFromCountry();
    }

    @GetMapping(value = "/api/customers/highSpenders")
    public ArrayList<CustomerSpender> getHighSpenders() {
        return customerRepository.getHighSpenders();
    }

    @GetMapping("/api/customers/{id}/popular/genre")
    public ArrayList<Genre> getPopularGenre(@PathVariable String id) {
        return customerRepository.getPopularGenre(id);
    }

    @GetMapping(value = "/api/customers", params = {"limit", "offset"})
    public ArrayList<Customer> getCustomerPage(@RequestParam int limit, @RequestParam int offset) {
        return customerRepository.getCustomerPage(limit, offset);
    }
}
