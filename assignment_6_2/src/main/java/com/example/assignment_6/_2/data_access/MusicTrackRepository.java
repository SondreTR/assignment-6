package com.example.assignment_6._2.data_access;

import com.example.assignment_6._2.models.Artist;
import com.example.assignment_6._2.models.Genre;
import com.example.assignment_6._2.models.Track;

import java.util.ArrayList;

public interface MusicTrackRepository {
    ArrayList<Genre> getRandomGenres(int genresAmount);
    ArrayList<Artist> getRandomArtist(int artistAmount);
    ArrayList<Track> getRandomTracks(int tracksAmount);
    ArrayList<Track> getTrackByName(String name);
}

