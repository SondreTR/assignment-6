package com.example.assignment_6._2.models;

public class Track {
    private int id;
    private String name;
    private Artist artist;
    private Album album;
    private Genre genre;

    public Track(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Track(int id, String name, Artist artist, Album album, Genre genre) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }

    public int getId() { return id; }
    public String getName() { return name; }
    public Artist getArtist() { return artist; }
    public Album getAlbum() { return album; }
    public Genre getGenre() { return genre; }

    public void setArtist(Artist artist) { this.artist = artist; }
    public void setAlbum(Album album) { this.album = album; }
    public void setGenre(Genre genre) { this.genre = genre; }
    public void setName(String name) { this.name = name; }
    public void setId(int id) { this.id = id; }


}

