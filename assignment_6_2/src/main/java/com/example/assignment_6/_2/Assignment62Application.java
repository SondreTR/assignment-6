package com.example.assignment_6._2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Assignment62Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment62Application.class, args);
	}

}
