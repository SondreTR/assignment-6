package com.example.assignment_6._2.data_access;

import com.example.assignment_6._2.logging.LogToConsole;
import com.example.assignment_6._2.models.Album;
import com.example.assignment_6._2.models.Artist;
import com.example.assignment_6._2.models.Genre;
import com.example.assignment_6._2.models.Track;
import org.springframework.stereotype.Repository;
import java.sql.*;
import java.util.ArrayList;

@Repository
public class MusicTrackRepositoryImpl implements MusicTrackRepository {

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;
    private final LogToConsole logger;

    public MusicTrackRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    public ArrayList<Genre> getRandomGenres(int genresAmount) {

        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Genreid, Name FROM genre ORDER BY RANDOM() LIMIT ?;");

            preparedStatement.setInt(1, genresAmount);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { genres.add(new Genre(resultSet.getInt("GenreId"), resultSet.getString("Name"))); }

        } catch (Exception exception) {
            logger.log(exception.toString());

        } finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return genres;
    }

    public ArrayList<Artist> getRandomArtist(int artistAmount) {
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Artistid, Name FROM artist ORDER BY RANDOM() LIMIT ?;");
            preparedStatement.setInt(1, artistAmount);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { artists.add(new Artist(resultSet.getInt("ArtistId"), resultSet.getString("Name"))); }

        } catch (Exception exception) {
            logger.log(exception.toString());

        } finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return artists;
    }

    public ArrayList<Track> getRandomTracks(int tracksAmount) {
        ArrayList<Track> tracks = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Trackid, Name FROM track ORDER BY RANDOM() LIMIT ?;");
            preparedStatement.setInt(1, tracksAmount);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { tracks.add(new Track(resultSet.getInt("Trackid"), resultSet.getString("Name"))); }

        } catch (Exception exception) {
            logger.log(exception.toString());

        } finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        return tracks;
    }

    public ArrayList<Track> getTrackByName(String name) {

        ArrayList<Track> tracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement =
                    conn.prepareStatement(
                            "SELECT track.Trackid, track.Name, artist.Artistid, artist.Name, album.Albumid, album.Title, genre.Genreid, genre.Name " +
                                "FROM track INNER JOIN album ON track.AlbumId = album.AlbumId INNER JOIN genre ON track.genreId = genre.genreId " +
                                "INNER JOIN artist ON album.ArtistId = artist.ArtistId WHERE track.Name LIKE ?;");

            preparedStatement.setString(1, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                new Artist(resultSet.getInt(3), resultSet.getString(4)),
                                new Album(resultSet.getInt(5), resultSet.getString(6)),
                                new Genre(resultSet.getInt(7), resultSet.getString(8))
                        )
                );
            }
        } catch (Exception exception) { logger.log(exception.toString());
        } finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return tracks;
    }
}
