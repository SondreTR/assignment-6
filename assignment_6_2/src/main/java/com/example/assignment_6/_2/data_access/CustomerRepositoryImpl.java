package com.example.assignment_6._2.data_access;

import com.example.assignment_6._2.logging.LogToConsole;
import com.example.assignment_6._2.models.Customer;
import com.example.assignment_6._2.models.CustomerCountry;
import com.example.assignment_6._2.models.CustomerSpender;
import com.example.assignment_6._2.models.Genre;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/*
 This class serves as the encapsulation of all database interactions,
 it removes the implementation from the controllers to models - as they should only be responsible
 for handling user interactions and deciding what to do with it.
*/
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final LogToConsole logger;

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    /*
     Need methods to serve the needs of the controller requests.
     Just mirror what your endpoints want.
    */
    public ArrayList<Customer> getAllCustomers(){

        ArrayList<Customer> customers = new ArrayList<>();

        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getInt("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        return customers;
    }

    public Customer getCustomerById(String customerId){
        Customer customer = null;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1,customerId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");
        }
        catch (Exception exception){ logger.log(exception.toString()); }
        finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return customer;
    }

    public ArrayList<Customer> getCustomersByName(String name){

        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE FirstName LIKE ? OR LastName LIKE ?");
            preparedStatement.setString(1,"%" + name + "%");
            preparedStatement.setString(2,"%" + name + "%");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
            logger.log("Select specific customer successful");
        }
        catch (Exception exception){ logger.log(exception.toString()); }
        finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return customers;
    }

    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?)");

//            preparedStatement.setString(1,customer.getCustomerID());
            preparedStatement.setString(1,customer.getFirstName());
            preparedStatement.setString(2,customer.getLastName());
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setInt(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhone());
            preparedStatement.setString(6,customer.getEmail());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?,  Phone = ?, Email = ? WHERE CustomerId = ?");

            preparedStatement.setInt(1,customer.getCustomerID());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setInt(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhone());
            preparedStatement.setString(7,customer.getEmail());
            preparedStatement.setInt(8,customer.getCustomerID());

            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public ArrayList<Customer> getCustomerPage(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement("select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from customer LIMIT ? OFFSET ?;");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }logger.log("Selecting specific number of customers was successful");
        }
        catch (Exception exception){ logger.log(exception.toString()); }
        finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return customers;
    }

    public ArrayList<CustomerCountry> getCustomerCountFromCountry() {

        ArrayList<CustomerCountry> customersCountry = new ArrayList<>();

        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT (*) AS Number FROM customer GROUP BY Country ORDER BY Number DESC;");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { customersCountry.add(new CustomerCountry(resultSet.getString("Country"), resultSet.getInt("Number"))); }
            logger.log("Selecting specific count of customers by country was successful");

        } catch (Exception exception) { logger.log(exception.toString());
        } finally {
            try { conn.close(); }
            catch (Exception exception) { logger.log(exception.toString()); }
        }
        return customersCountry;
    }

    public ArrayList<CustomerSpender> getHighSpenders() {
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT customer.CustomerId, customer.FirstName, customer.LastName, customer.Country, customer.PostalCode, customer.Phone, customer.Email, sum(invoice.Total) as sumTotal from Customer inner join invoice on customer.CustomerId = invoice.CustomerId GROUP BY customer.CustomerId ORDER BY sumTotal DESC;");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerSpenders.add(new CustomerSpender(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getInt("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")),
                        resultSet.getDouble("sumTotal")
                ));
            }logger.log("Selecting high-spenders was successful");
        }
        catch (Exception exception){ logger.log(exception.toString()); }
        finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return customerSpenders;
    }

    public ArrayList<Genre> getPopularGenre(String id) {

        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement preparedStatement = conn.prepareStatement(
                    "SELECT GenreIds, GenreName, MAX(GenreCount) FROM (SELECT customer.CustomerId as ID, genre.GenreId as GenreIds, genre.Name as GenreName, " +
                        "COUNT(genre.GenreId) as GenreCount FROM customer inner join invoice on ID = invoice.CustomerId inner join invoiceLine on " +
                        "invoice.InvoiceId = invoiceLine.InvoiceId inner join track on invoiceLine.TrackId = track.TrackId inner join genre on track.GenreId = genre.GenreId " +
                        "where ID = ? GROUP BY genre.GenreId) a GROUP BY ID;");

            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(new Genre(
                        resultSet.getInt("GenreIds"),
                        resultSet.getString("GenreName")
                ));
            }
            logger.log("Getting popular genre was successful");
        }
        catch (Exception exception){ logger.log(exception.toString()); }
        finally {
            try { conn.close(); }
            catch (Exception exception){ logger.log(exception.toString()); }
        }
        return genres;
    }
}
