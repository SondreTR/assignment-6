package com.example.assignment_6._2.data_access;

import com.example.assignment_6._2.models.Customer;
import com.example.assignment_6._2.models.CustomerCountry;
import com.example.assignment_6._2.models.CustomerSpender;
import com.example.assignment_6._2.models.Genre;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String customerId);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(Customer customer);
    public ArrayList<Customer> getCustomersByName(String name);

    ArrayList<CustomerCountry> getCustomerCountFromCountry();
    ArrayList<CustomerSpender> getHighSpenders();
    ArrayList<Genre> getPopularGenre(String customerID);
    ArrayList<Customer> getCustomerPage(int limit, int offset);
}

